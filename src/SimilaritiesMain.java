import agents.Similarities;
import agents.StateAction;
import game.Session;

import java.util.HashSet;

/*
Total states in hash set: 166898

REAL
Total states in hash set: 96318
Total similarities: 1873400
STATE
Total states in hash set: 88744
Total similarities: 630474
 */

/**
 * Class SimilaritiesMain - used to build and save similarities.
 */
public class SimilaritiesMain {

    /**
     * Run to achieve a set of visited states.
     *
     * @param COUNT total sessions interations
     * @return the set of visited StateAction
     */
    private static HashSet<StateAction> runForStates(final int COUNT) {
        int trainCount = 0;
        Session session = new Session();
        session.create(Main.WIDTH, Main.HEIGHT);

        while (trainCount <= COUNT) {
            // run a session
            session.run();
            trainCount++;
        }

        System.out.println("End of run");
        HashSet<StateAction> states = session.getAgent().getQvalue().getStates();
        System.out.println("Total states in hash set: " + states.size());

        return states;
    }

    /**
     * Main function to build similarities data file.
     * @param args the args
     */
    public static void main(String[] args) {
        final String FILE_NAME = "table.ser";
        System.out.println("Build begin");
        Similarities similarities = new Similarities(Main.WIDTH, Main.HEIGHT);
        similarities.buildRandom(runForStates(50000));
        similarities.keep(FILE_NAME);
        System.out.println("Build end");
    }
}
