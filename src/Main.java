
import game.Session;
import game.SessionProxy;

/**
 * Class Main (Operator).
 */
public class Main {
    // game constants
    private static final int SIZE = 7;
    public static final int WIDTH = SIZE;
    public static final int HEIGHT = SIZE;

    private static int runTest(Session session) {
        int trainCount = 1, sum = 0, avg = -1;
        final int TRAIN_TOTAL = 10000, BATCH = TRAIN_TOTAL * 10 / 100;
        session.create(WIDTH, HEIGHT);

        System.out.println("Begin training");

        while (trainCount <= TRAIN_TOTAL) {
            // run a session
            session.run();

            // show avg
            if (trainCount % BATCH == 0) {
                avg = (sum / BATCH);
                System.out.println("Avg moves: " + avg);
                sum = 0;
            } else {
                sum += session.getMovesCount();
            }

            trainCount++;
        }

        System.out.println("End training");

        return avg;
    }

    /**
     * main function.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        int sum = 0, COUNT = 10;
        Session session = new Session();
        session.create(WIDTH, HEIGHT);
        session.setAgentType(SessionProxy.TYPE_SIMILARITY);

        for (int i = 1; i <= COUNT; i++) {
            System.out.println("----- Running test number " + i + " -----");
            sum += runTest(session);
            System.out.println();
        }

        System.out.println("Avg: " + (sum/COUNT));
    }
}
