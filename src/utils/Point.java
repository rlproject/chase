package utils;

import java.io.Serializable;

/**
 * Class Point - represents a (mutable) point in a two dimensional space.
 *
 * Note: the points coordinates are represented as bytes to save space.
 */
public class Point implements Cloneable, Serializable {
    public static final Point ZERO = new Point(0, 0);

    private byte x;  // the x-axis coord
    private byte y;  // the y-axis coord

    /**
     * Constructs the point.
     *
     * @param x the x-axis coordinates
     * @param y the y-axis coordinates
     */
    public Point(int x, int y) {
        this.x = (byte) x;
        this.y = (byte) y;
    }

    /**
     * Gets the x-axis coordinates.
     *
     * @return the x-axis coordinates
     */
    public int getX() {
        return this.x;
    }

    /**
     * Gets the y-axis coordinates.
     *
     * @return the y-axis coordinates
     */
    public int getY() {
        return this.y;
    }

    /**
     * Translates the point.
     *
     * @param x the x-axis value
     * @param y the y-axis value
     * @return the shifted point by x and y
     */
    public Point translate(int x, int y) {
        return new Point(this.x + x, this.y + y);
    }

    /**
     * Translates the point.
     *
     * @param diff the dx dy as point
     * @return the shifted point by the diff
     */
    public Point translate(Point diff) {
        return translate(diff.getX(), diff.getY());
    }

    /**
     * Returns a point vector to a destination point.
     *
     * @param to the destination point
     * @return the vector point from us to the destination vector
     */
    public Point vector(Point to) {
        return new Point(to.x - this.x, to.y - this.y);
    }

    /**
     * Flips the y-axis coordinates.
     *
     * @return the same point with y-axis flipped
     */
    public Point flipY() {
        return new Point(this.x, -1 * this.y);
    }

    /**
     * Returns the angle to another point (vector).
     *
     * @param target the target point
     * @return the angle between the vectors
     */
    public double angle(Point target) {
        double angle = Math.toDegrees(Math.atan2(target.y - y, target.x - x));

        if(angle < 0){
            angle += 360;
        }

        return angle;
    }

    /**
     * Calculates the distance between the points.
     *
     * @param p the other point
     * @return the distance between the points
     */
    public double distance(Point p) {
        return Math.sqrt(Math.pow(p.x - this.x, 2) + Math.pow(p.y - this.y, 2));
    }

    /**
     * Returns the hash code of the point.
     *
     * @return the hash code of the point
     */
    @Override
    public int hashCode() {
        int hash = x;
        hash = 13 * hash + y;

        return hash;
    }

    /**
     * Checks if another object is equal to this.
     *
     * @param other the other object
     * @return true if the other object is an equal point, false otherwise
     */
    @Override
    public boolean equals(Object other) {
        if (other instanceof Point) {
            Point otherPoint = (Point) other;
            return this.x == otherPoint.x && this.y == otherPoint.y;
        }

        return false;
    }

    @Override
    public String toString() {

        return String.format("(%d, %d)", x, y);
    }
    public Point get() {
        Point cloned = null;
        try {
            cloned = (Point) this.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println("CloneNotSupportedException");
        }
        return cloned;
    }

    /**
     * Calculates the dot-product.
     *
     * @param other the other point (vector)
     * @return the dot-product
     */
    public Double product(Point other) {
        return ZERO.distance(this) * ZERO.distance(other) * Math.cos(this.angle(other));
    }
}
