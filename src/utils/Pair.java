package utils;

import java.io.Serializable;

/**
 * Class Pair - represents a pair.
 */
public class Pair<L,R> {
    private final L left;
    private final R right;

    /**
     * Constructor.
     *
     * @param left the first arg
     * @param right the second arg
     */
    public Pair(L left, R right) {
        this.left = left;
        this.right = right;
    }

    /**
     * Gets left object.
     *
     * @return the left object
     */
    public L getLeft() { return left; }

    /**
     * Gets the right object.
     *
     * @return the right object
     */
    public R getRight() { return right; }

    @Override
    public int hashCode() { return left.hashCode() ^ right.hashCode(); }

    /**
     * Checks if this is equal to a given object.
     *
     * @param other the other object
     * @return true if they are equal, false otherwise
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Pair)) return false;
        Pair pairo = (Pair) other;
        return this.left.equals(pairo.getLeft()) &&
                this.right.equals(pairo.getRight());
    }
}
