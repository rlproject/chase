package agents;

import utils.Point;

import java.awt.geom.AffineTransform;
import java.io.*;
import java.util.*;

/**
 * Class Similarities - the similarities collection builder and holder.
 */
public class Similarities {
    private int width;
    private int height;
    private HashMap<StateAction, ArrayList<StateAction>> table;

    /**
     * Constructs the Similarities.
     *
     * @param width  the width
     * @param height the height
     */
    public Similarities(int width, int height) {
        this.width = width;
        this.height = height;
        this.table = new HashMap<>();
    }

    /**
     * Adds similarity of first to second.
     *
     * @param sa1 the first
     * @param sa2 the second
     */
    private void addSimilarityOrdered(StateAction sa1, StateAction sa2) {
        ArrayList<StateAction> sims = table.get(sa1);

        // still no similarities
        if (sims == null) {
            sims = new ArrayList<>();
            table.put(sa1, sims);
        }

        // add current
        sims.add(sa2);
    }

    /**
     * Adds symmetric similarity.
     *
     * @param sa1 one similarity
     * @param sa2 the other
     */
    private void addSimilarity(StateAction sa1, StateAction sa2) {
        addSimilarityOrdered(sa1, sa2);
        addSimilarityOrdered(sa2, sa1);
    }

    /**
     * Checks if a state is legal.
     *
     * @param s the state
     * @return true if the state is legal, false otherwise
     */
    private boolean isLegal(State s) {
        Point s1 = s.getPredatorPosition(0);
        Point s2 = s.getPredatorPosition(1);
        Point p = s.getPreyPosition();
        return isInBounds(s1) && isInBounds(s2) && isInBounds(p)
                && !s1.equals(s2) && !s1.equals(p) && !s2.equals(p);
    }

    /**
     * Checks if a give location is in the bounds of the board.
     *
     * @param location the location to check
     * @return true if the location is in bounds, false otherwise
     */
    private boolean isInBounds(Point location) {
        return location.getX() >= 0 && location.getX() < width && location.getY() >= 0 && location.getY() < height;
    }

    /**
     * Applies noise on a given point.
     *
     * @param origin  the StateAction
     * @param ACTIONS the actions collection
     * @param rand    the randomizer
     * @return the noised point
     */
    private Point getNoisedPoint(Point origin, Random rand, List<Action> ACTIONS) {
        return origin.translate(ACTIONS.get(rand.nextInt(ACTIONS.size())).getAction());
    }

    /**
     * Applies noise on a given state.
     *
     * @param sa      the StateAction
     * @param ACTIONS the actions collection
     * @param rand    the randomizer
     * @return the noised StateAction
     */
    private StateAction noise(StateAction sa, List<Action> ACTIONS, Random rand) {
        Point preyPos = getNoisedPoint(sa.getState().getPreyPosition(), rand, ACTIONS);
        Point pred1Pos = getNoisedPoint(sa.getState().getPredatorPosition(0), rand, ACTIONS);
        Point pred2Pos = getNoisedPoint(sa.getState().getPredatorPosition(1), rand, ACTIONS);

        return new StateAction(new State(preyPos, new Point[]{pred1Pos, pred2Pos}), sa.getActions());
    }

    /**
     * Rotates the given point by the given angle.
     *
     * @param p     the point
     * @param ANGLE the angle in degrees
     * @return the rotated point
     */
    private Point getRotatedPoint(Point p, final double ANGLE) {
        double[] pt = {p.getX(), p.flipY().getY()};
        AffineTransform.getRotateInstance(Math.toRadians(ANGLE), this.width / 2.0, -this.height / 2.0)
                .transform(pt, 0, pt, 0, 1);
        return new Point((int) pt[0], (int) pt[1]).flipY();
    }

    /**
     * Rotates the StateAction by the given angle.
     *
     * @param sa    the StateAction
     * @param ANGLE the angle in degrees
     * @return the rotates StateAction
     */
    private StateAction rotate(StateAction sa, final double ANGLE) {
        Actions rotatedAcs = new Actions(sa.getActions().getA1().rotate(ANGLE), sa.getActions().getA2().rotate(ANGLE));

        return new StateAction(new State(getRotatedPoint(sa.getState().getPreyPosition().get(), ANGLE),
                new Point[]{getRotatedPoint(sa.getState().getPredatorPosition(0).get(), ANGLE),
                        getRotatedPoint(sa.getState().getPredatorPosition(1).get(), ANGLE)}),
                rotatedAcs);
    }

    /**
     * Randomly build the table.
     * Apply noise and rotations on given states to get similarities.
     *
     * @param R the StateAction collection to build from
     */
    public void buildRandom(HashSet<StateAction> R) {
        final int ROTATIONS_COUNT = 4;
        final double ANGLE = 360.0 / ROTATIONS_COUNT;
        final List<Action> ACTIONS = Action.actionSet();
        Random rand = new Random();
        // When true State-Action similarity. When false - State-similarity.
        boolean stateActionSim = true;

        for (StateAction p : R) {
            // apply noise on p
            StateAction pN = noise(p, ACTIONS, rand);
            StateAction pN2 = noise(p, ACTIONS, rand);
            StateAction pN3 = noise(p, ACTIONS, rand);

            if (stateActionSim) {
                for (int i = 1; i <= ROTATIONS_COUNT; i++) {
                    double ithAngle = ANGLE * i;

                    // apply rotation
                    StateAction pR = rotate(p, ithAngle);
                    StateAction pNR = rotate(pN, ithAngle);
                    StateAction pNR2 = rotate(pN2, ithAngle);
                    StateAction pNR3 = rotate(pN3, ithAngle);

                    // legality flag after noise and rotation
                    boolean isLegalPR = isLegal(pR.getState());
                    boolean isLegalPNR = isLegal(pNR.getState());
                    boolean isLegalPNR2 = isLegal(pNR2.getState());
                    boolean isLegalPNR3 = isLegal(pNR3.getState());

                    // go over valid combinations to add similarities

                    if (isLegalPR) {
                        if (isLegalPNR) {
                            addSimilarity(pR, pNR);
                        }

                        if (isLegalPNR2) {
                            addSimilarity(pR, pNR2);
                        }

                        if (isLegalPNR3) {
                            addSimilarity(pR, pNR3);
                        }

                        if (isLegalPNR && isLegalPNR2) {
                            addSimilarity(pNR, pNR2);
                        }

                        if (isLegalPNR2 && isLegalPNR3) {
                            addSimilarity(pNR2, pNR3);
                        }

                        if (isLegalPNR && isLegalPNR3) {
                            addSimilarity(pNR, pNR3);
                        }
                    }
                }
            } else {
                // State similarity
                // legality flag after noise
                boolean isLegalP = isLegal(p.getState());
                boolean isLegalPN = isLegal(pN.getState());
                boolean isLegalPN2 = isLegal(pN2.getState());
                boolean isLegalPN3 = isLegal(pN3.getState());

                // go over valid combinations to add noise-similarities

                if (isLegalP) {
                    if (isLegalPN) {
                        addSimilarity(p, pN);
                    }

                    if (isLegalPN2) {
                        addSimilarity(p, pN2);
                    }

                    if (isLegalPN3) {
                        addSimilarity(p, pN3);
                    }
                }

                if (isLegalPN && isLegalPN2) {
                    addSimilarity(pN, pN2);
                }

                if (isLegalPN2 && isLegalPN3) {
                    addSimilarity(pN2, pN3);
                }

                if (isLegalPN && isLegalPN3) {
                    addSimilarity(pN, pN3);
                }

            }
        }

        // show total
        long sum = 0;
        for (Map.Entry<StateAction, ArrayList<StateAction>> e : table.entrySet()) {
            ArrayList<StateAction> sims = table.get(e.getKey());
            sum += sims.size();
        }
        System.out.println("Total similarities: " + sum);
    }


    /**
     * Load similarities from file
     *
     * @param file
     */
    public void load(String file) {
        try {
            FileInputStream fileIn = new FileInputStream(file);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            table = (HashMap<StateAction, ArrayList<StateAction>>) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("Table not found");
            c.printStackTrace();
        }
    }

    public void unload() {
        this.table = new HashMap<>();
    }

    /**
     * Write the similarities to file
     *
     * @param file
     */
    public void keep(String file) {
        try {
            FileOutputStream fout = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(table);
            oos.close();
            System.out.println("Done saving");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Gets the table of similarities.
     *
     * @return the table
     */
    public HashMap<StateAction, ArrayList<StateAction>> getTable() {
        return table;
    }
}
