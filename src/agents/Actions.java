package agents;


import java.io.Serializable;

/**
 * Class Actions - represents a pair of actions.
 */
public class Actions implements Serializable {
    private Action a1;
    private Action a2;

    public Actions(Action a1, Action a2) {
        this.a1 = a1;
        this.a2 = a2;
    }

    /**
     * @return the first predator's action
     */
    public Action getA1() {
        return a1;
    }

    /**
     * @return the second predator's action
     */
    public Action getA2() {
        return a2;
    }

    @Override
    public String toString() {
        return String.format("Actions: %s\t%s", a1.toString(), a2.toString());
    }

    /**
     * Checks if this equals another object.
     *
     * @param other the other object
     * @return true if the actions are equal, false otherwise
     */
    @Override
    public boolean equals(Object other) {
        if (other instanceof Actions) {
            Actions otherAction = (Actions) other;
            return otherAction.getA1().equals(this.getA1()) && otherAction.getA2().equals(this.getA2());
        }

        return false;
    }
}
