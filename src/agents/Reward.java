package agents;

import utils.Point;

/**
 * Class Reward - holds functions that calculate reward.
 */
public class Reward {
    /**
     * Encourages a predator to move closer to the prey.
     *
     * @param predatorPos the predator's position
     * @param preyPos the prey's position
     * @return the negative of the distance between the predator and the prey
     */
    public static double proximity(Point predatorPos, Point preyPos) {
        return predatorPos.distance(preyPos);
    }

    /**
     * Encourages the predators to move to different sides of the prey, encircling it.
     *
     * @param prey the prey's position
     * @param pred1 the first predator position
     * @param pred2 the second predator position
     * @return the angle
     */
    public static double angle(Point prey, Point pred1, Point pred2) {
        Point x = prey.flipY().vector(pred1.flipY());
        Point y = prey.flipY().vector(pred2.flipY());
        Double xy = x.product(y);
        Double normXY = x.distance(Point.ZERO) * y.distance(Point.ZERO);

        return Math.acos(xy / normXY);
    }

    /**
     * Encourages the predators to move away from each other.
     *
     * @param pred1 the first predator's position
     * @param pred2 the second predator's position
     * @return
     */
    public static double separation(Point pred1, Point pred2) {
        return pred1.distance(pred2);
    }

    /**
     * Calculates the reward of the current (state, action, state').
     *
     * @return the reward of the current (state, action, state').
     */
    public static double calculate(State gameState, int predatorIndex) {
        // shape rewards
        double sizeTwice = gameState.getWidth() + gameState.getHeight();
        double pReward = (sizeTwice - proximity(gameState.getPredatorPosition(predatorIndex), gameState.getPreyPosition())) / sizeTwice;
        double aReward = angle(gameState.getPreyPosition(), gameState.getPredatorPosition(0), gameState.getPredatorPosition(1)) / Math.PI;
        double sReward = separation(gameState.getPredatorPosition(0), gameState.getPredatorPosition(1)) / sizeTwice;

        // goal reward
        double winReward = 0.0;

        if (gameState.won()) {
            winReward = sizeTwice;
        }
        // System.out.println("p: " + pReward + "a: " + aReward + "s: " + sReward);
        return 0.75 * pReward + 0.15 * aReward + 0.15 * sReward + winReward; // total
    }
}
