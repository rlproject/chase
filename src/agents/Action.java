package agents;

import utils.Point;

import java.io.Serializable;
import java.util.*;

/**
 * Class Action - represents an action.
 */
public class Action implements Cloneable, Serializable {
    private static final Action[] ROTATABLE;
    private static final String[] DESCRIPTIONS;
    static {
        ROTATABLE = new Action[]{Action.up(), Action.left(), Action.down(), Action.right()};
        DESCRIPTIONS = new String[]{"up", "left", "down", "right", "stay"};
    }

    private Point direction;    // the direction

    /**
     * Constructs an action.
     *
     * @param direction the direction
     */
    private Action(Point direction) {
        this.direction = direction;
    }

    public static Action left() {
        return new Action(new Point(-1, 0));
    }

    public static Action right() {
        return new Action(new Point(+1, 0));
    }

    public static Action up() {
        return new Action(new Point(0, -1));
    }

    public static Action down() {
        return new Action(new Point(0, +1));
    }

    public static Action stay() {
        return new Action(new Point(0, 0));
    }

    private String getDescription() {
        return DESCRIPTIONS[this.getNumber()];
    }

    /**
     * Gets the action.
     *
     * @return the action as a point
     */
    public Point getAction() { return this.direction; }

    /**
     * Sets the direction.
     *
     */
    private void setDirection(Point d) { this.direction = d; }

    @Override
    public int hashCode() {
        return getDescription().hashCode();
    }

    /**
     * Checks if this equals another object.
     *
     * @param other the other object
     * @return true if the actions are equal, false otherwise
     */
    @Override
    public boolean equals(Object other) {
        if (other instanceof Action) {
            Action otherAction = (Action) other;
            return otherAction.direction.equals(this.direction);
        }

        return false;
    }

    /**
     * Returns a string representation of the action.
     *
     * @return the string representation of the action
     */
    @Override
    public String toString() {
        return this.getDescription();
    }
    /**
     * Returns a cloned action.
     *
     * @return a cloned action
     */
    public Action get() {
        Action cloned = null;
        try {
            cloned = (Action) this.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println("CloneNotSupportedException");
        }
        return cloned;
    }

    /**
     * Returns a cloned object.
     *
     * @return a cloned object
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        Action cloned = (Action) super.clone();
        cloned.setDirection(direction.get());
        return cloned;
    }
    // TODO: make singleton?
    public static List<Action> actionSet() {
        return Arrays.asList(Action.left(), Action.right(), Action.up(), Action.down(), Action.stay());
    }

    public static List<Actions> actionsSet() {
       List<Actions> ret = new ArrayList<>();
        List<Action> actions = Arrays.asList(Action.left(), Action.right(), Action.up(), Action.down(), Action.stay());
        for (Action a1 : actions) {
            for (Action a2 : actions) {
                ret.add(new Actions(a1, a2));
            }
        }
        return ret;
    }

    /**
     * Gets the number of the action.
     *
     * @return the number of the action
     */
    private int getNumber() {
        if (this.equals(up())) {
            return 0;
        }

        if (this.equals(left())) {
            return 1;
        }

        if (this.equals(down())) {
            return 2;
        }

        if (this.equals(right())) {
            return 3;
        }

        return 4;
    }

    /**
     * Rotates the action by the given angle.
     * @param angle the angle
     * @return the rotated action
     */
    public Action rotate(double angle) {
        if (this.equals(stay())) {
            return stay();
        }

        int noOfRotations = (int) (angle / 360) * 8;

        return ROTATABLE[(this.getNumber() + noOfRotations) % 4].get();
    }
}
