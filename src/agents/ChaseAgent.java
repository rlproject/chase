package agents;

import utils.Pair;

/**
 * Interface ChaseAgent - represents an agent that can play Chase.
 */
public interface ChaseAgent extends Agent {

    /**
     * Observes the game state.
     *
     * @param gameState the current game state
     */
    void observe(State gameState);

    /**
     * Returns the next move of the agent.
     *
     * @return the next action of the agent
     */
     Actions getNextMove();
}
