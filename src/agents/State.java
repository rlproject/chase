package agents;

import utils.Point;
import java.io.Serializable;

/**
 * Class State - represents a game state.
 */
public class State implements Cloneable, Serializable {
    // the game's properties
    public static int width, height;
    private Point prey;
    private Point[] predators;
    private final boolean win;

    /**
     * @param w - width
     * @param h - height
     */
    public static void init(int w, int h) {
        width = w;
        height = h;
    }

    /**
     *
     * @param prey
     * @param predators
     */
    public State(Point prey, Point[] predators) {
        this(prey, predators, false);
    }

    /**
     * Constructs a game state.
     *
     * @param prey the prey
     * @param predators the predator
     * @param win the win state
     */
    public State(Point prey, Point[] predators, boolean win) {
        this.prey = prey;
        this.predators = predators;
        this.win = win;
    }

    /**
     * @param i - predator index
     * @param p - location
     */
    public void setPredators(int i, Point p) {
        this.predators[i] = p;
    }

    /**
     * @param p locations of the predators
     */
    public void setPredators(Point[] p) {
        this.predators = p;
    }

    /**
     * @param p
     */
    public void setPrey(Point p) {
        this.prey = p;
    }

    /**
     * Gets the prey's position.
     *
     * @return the prey's position
     */
    public Point getPreyPosition() {
        return prey.get();
    }

    /**
     * Gets the predator position.
     *
     * @param i the ith predator
     * @return the ith predator's position
     */
    public Point getPredatorPosition(int i) {
        return predators[i].get();
    }

    /**
     * Gets the width of the game.
     *
     * @return the width of the game
     */
    public int getWidth() {
        return width;
    }

    /**
     * Gets the height of the game.
     *
     * @return the height of the game
     */
    public int getHeight() {
        return height;
    }

    /**
     * Checks if the game has ended.
     *
     * @return true if the game has ended, false otherwise
     */
    public boolean won() { return this.win; }

    /**
     * Checks if this equals another object.
     *
     * @param other the other object
     * @return true if the states are equal, false otherwise
     */
    @Override
    public boolean equals(Object other) {
        if (other instanceof State) {
            State otherState = (State) other;
            return otherState.prey.get().equals(this.prey.get())
                    && ((this.predators[0].get().equals(otherState.getPredatorPosition(0))
                        && this.predators[1].get().equals(otherState.getPredatorPosition(1))));
        }

        return false;
    }

    @Override
    public String toString() {
        return String.format("Prey: %s\nPredators: %s\t%s", prey.get().toString(),
                                                            predators[0].get().toString(),
                                                            predators[1].get().toString());
    }
    /**
     * Returns a cloned State.
     *
     * @return a cloned State
     */
    public State get() {
        State cloned = null;
        try {
            cloned = (State) this.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println("CloneNotSupportedException");
        }
        return cloned;
    }
    /**
     * Returns a cloned object.
     *
     * @return a cloned object
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        State cloned = (State) super.clone();
        cloned.setPrey((Point)cloned.getPreyPosition().get());
        Point[] p = {predators[0].get(), predators[1].get()};
        cloned.setPredators(p);
        return cloned;
    }

    public State swapPredators() {
        State s = this.get();

        // do swap
        Point p0 = s.getPredatorPosition(0);
        s.setPredators(0, s.getPredatorPosition(1));
        s.setPredators(1, p0);

        return  s;
    }
}
