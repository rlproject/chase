package agents;

import java.util.*;

/**
 * Class Qvalue - holds the knowledge.
 */
public class Qvalue {
    private static final double ALPHA = 0.5;    // learning rate
    private static final double BETA0 = 1;      // decay of similarities const
    private double BETA;                    // decay of similarities
    private Map<StateAction, Double> values;    // the value function
    private Similarities similarities;          // the similarities table
    private int time;                       // time (moves)

    /**
     * Constructs Qvalue.
     */
    public  Qvalue(Similarities similarities) {
        this.values = new HashMap<>();
        this.similarities = similarities;
        this.BETA = 1.0;
        this.time = 1;
    }

    /**
     * Gets the states.
     *
     * @return a set of the state and action pairs
     */
    public HashSet<StateAction> getStates() {
        HashSet<StateAction> states = new HashSet<>();

        for (Map.Entry<StateAction, Double> e : values.entrySet()) {
            states.add(e.getKey());
        }

        return states;
    }

    /**
     * Calculates the next best action according to the Qvalues.
     *
     * @param state   the current state of the agent.
     * @param actions the possible actions the agent can make.
     * @return the best actions.
     */
    public List<Actions> getBestAction(State state, List<Actions> actions) {
        final List<Actions> ACTIONS_SET = Action.actionsSet();

        // find max Qvalue from (state, action) from all actions
        double max = Double.MIN_VALUE;
        ArrayList<Actions> topActions = new ArrayList<>();
        for (Actions a : actions) {
                StateAction stateAction = new StateAction(state, a);
                if (values.containsKey(stateAction)) {
                    // that's the state after applying the action a
                    State afterAction = stateAction.getState().get();
                    double expectedSum = values.get(stateAction);
                    if (expectedSum == max) {
                        topActions.add(a);
                    } else if (expectedSum > max) {
                        topActions = new ArrayList<>();
                        topActions.add(a);
                        max = expectedSum;
                    }
                }
            }
        if (topActions.size() == 0) {
            return actions;
        } else {
            return topActions;
        }
    }

    /**
     * Q(s,a) <- Q(s,a) + alpha(r+gamma*max_{a’} Q(s’,a’) - Q(s,a))
     for any <s~,a~> which have similarity c to <s,a> do:
     Q(s~,a~) <- Q(s~,a~) + alpha*beta_t *c * (r+gamma*max_{a’} Q(s’,a’) - Q(s~,a~))
     * @param sa1
     */
    public void updateSimilar(StateAction sa1, Double sample) {
        time++;
        if (!similarities.getTable().containsKey(sa1)) {
            return;
        }
        for (StateAction sa2 : similarities.getTable().get(sa1)) {
            double strength = 1.0;
            if (!values.containsKey(sa2)) {
                values.put(sa2.get(), strength * sample);
            } else {
                Double currentValue = values.get(sa2);
                // Q(s~,a~) <- Q(s~,a~) + alpha*beta_t *c * (r+gamma*max_{a’} Q(s’,a’) - Q(s~,a~))
                Double newValue = currentValue + strength * ALPHA * BETA * (sample - currentValue);
                values.put(sa2.get(), newValue);
            }
        }
        BETA = BETA0 * 1.0 / Math.pow(time, 0.2);
    }

    /**
     * Calculates the next best action according to the Qvalues.
     *
     * @param stateAction the state action pair
     * @param sample the observed value
     */
    public void update(StateAction stateAction, Double sample) {
        if (!values.containsKey(stateAction)) {
            values.put(stateAction.get(), sample);
        } else {
            Double currentValue = values.get(stateAction);
            Double newValue = currentValue + ALPHA * (sample - currentValue);
            values.put(stateAction.get(), newValue);
        }
    }

    private double getValueSafe(StateAction sa) {
        if (values.containsKey(sa)) {
            return values.get(sa);
        }

        return 0.0; // not exist
    }

    /**
     * Calculates maximum Qvalue from the given state with all possible actions.
     */
    // TODO: implement
    public double getMaxQvalue(State state, List<Actions> actions) {
        double max = 0.0;
        for (Actions a : actions) {
            StateAction stateAction = new StateAction(state, a);
            double myValue = getValueSafe(stateAction);
            if (myValue > max) {
                max = myValue;
            }
        }
        return max;
    }

    /**
     * Returns the size of the qvalue.
     *
     * @return the total elements in the qvalue.
     */
    public int size() {
        return this.values.size();
    }

    /**
     * @return a mapping of StateAction to its estimated value
     */
    public Map<StateAction, Double> getValues() {
        return values;
    }

    /**
     * @param v
     */
    public void loadValues(Map<StateAction, Double> v) {
        this.values = v;
    }

}
