package agents;

import utils.Point;

import java.io.Serializable;

/**
 * Class StateAction - represents a pair of state and action.
 */
public class StateAction implements Cloneable, Serializable {
    private State state;
    private Actions action;


    /**
     * @param state
     * @param action
     * Constructor
     */
    public StateAction(State state, Actions action) {
        this.state = state;
        this.action = action;
    }

    /**
     * @param state
     * @param a1
     * @param a2
     * Constructor
     */
    public StateAction(State state, Action a1, Action a2) {
        this.state = state;
        this.action = new Actions(a1, a2);
    }

    public void setState(State s) {
        this.state = s;
    }

    public void setActions(Actions a) {
        this.action = a;
    }

    public State getState() {
        return this.state;
    }

    public Actions getActions() {
        return this.action;
    }

    @Override
    public int hashCode() {
        int hash = 1;

        // we use the same prime for both predators to achieve symmetry
        hash = hash * 7 + state.getPredatorPosition(0).hashCode();
        hash = hash * 7 + state.getPredatorPosition(1).hashCode();
        hash = hash * 5 + state.getPreyPosition().hashCode();
        hash = hash * 3 + action.getA1().hashCode();
        hash = hash * 3 + action.getA2().hashCode();

        return hash;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof StateAction) {
            StateAction otherSA = (StateAction) other;
            return otherSA.state.equals(this.state) && otherSA.action.getA1().equals(this.action.getA1()) && otherSA.action.getA2().equals(this.action.getA2());
        }

        return false;
    }

    @Override
    public String toString() {

        return String.format("State: %s\nAction1: %s\nAction2: %s\n", state.toString(), action.getA1().toString(), action.getA2().toString());
    }
    /**
     * Returns a cloned StateAction.
     *
     * @return a cloned StateAction
     */
    public StateAction get() {
        StateAction cloned = null;
        try {
            cloned = (StateAction) this.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println("CloneNotSupportedException");
        }
        return cloned;
    }
    /**
     * Returns a cloned object.
     *
     * @return a cloned object
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        StateAction cloned = (StateAction) super.clone();
        cloned.setState(state.get());
        cloned.setActions(new Actions(action.getA1().get(), action.getA2().get()));
        return cloned;
    }

    /**
     * @return the new state after the actions are made
     */
    State GetNextState() {
        Point new_p1 = state.getPredatorPosition(0).translate(action.getA1().getAction());
        Point new_p2 = state.getPredatorPosition(1).translate(action.getA2().getAction());
        State new_state = state.get();
        new_state.setPredators(0, new_p1);
        new_state.setPredators(1, new_p2);
        return new_state;
    }
}
