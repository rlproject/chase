package agents;

import utils.Pair;

/**
 * Class RandomAgent - represents a basic chase agent that plays randomly.
 */
public class RandomAgent implements ChaseAgent {
    @Override
    public void observe(State gameState) {

    }

    @Override
    public Actions getNextMove() {
        return new Actions(Action.up(), Action.up());
    }

    @Override
    public void keepKnowledge(String file) {

    }

    @Override
    public void loadKnowledge(String file) {

    }

    @Override
    public void setMode(int mode) {

    }

    @Override
    public void prepare() {

    }
}
