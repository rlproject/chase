package agents;

import game.Session;
import game.SessionProxy;
import utils.Pair;
import utils.Point;


import java.io.*;
import java.util.*;

/**
 * Class QAgent - represents a chase agent that uses Q Learning inorder to play.
 */
public class QAgent implements ChaseAgent {
    private final double GAMMA = 0.8; // the discount factor
    private final int width, height;
    private Double lastSample; // the last sample
    // The previous state and action
    private StateAction lastStateAction;
    // The current state, after performing the action above from the state above.
    private State currentState;
    private Qvalue qvalue;
    private int mode;
    private Actions lastAction;
    private final int EXPLORE_THRESHOLD;


    /**
     * @param width
     * @param height
     * @param similarities
     * Construct QAgent
     */
    public QAgent(int width, int height, Similarities similarities) {
        this.width = width;
        this.height = height;
        this.qvalue = new Qvalue(similarities);
        this.lastStateAction = null;
        this.currentState = null;
        this.mode = SessionProxy.MODE_TRAIN;
        this.lastAction = new Actions (Action.stay(), Action.stay());
        this.EXPLORE_THRESHOLD = 10 * width * height;
    }

    /**
     * Reset fields to prepare for learning.
     */
    public void prepare() {
        this.lastStateAction = null;
        this.currentState = null;
    }

    /**
     * @return the last stateAsction and its update value
     */
    public Pair<StateAction, Double> getLastUpdate() {
        return new Pair<>(this.lastStateAction, lastSample);
    }

    @Override
    public void observe(State gameState) {
        if (lastStateAction == null) {
            this.lastStateAction = new StateAction(gameState, new Actions(Action.stay(), Action.stay()));
        }

        currentState = gameState;
        double reward = (Reward.calculate(gameState, 0) + Reward.calculate(gameState, 1))/2;
        double sample = reward + GAMMA * qvalue.getMaxQvalue(gameState, candidateActions(gameState));
        lastSample = sample;
        StateAction sa = lastStateAction.get();

        if (this.mode == SessionProxy.MODE_TRAIN) {
            update(sa, sample);
        }
    }

    private void update(StateAction sa, double sample) {
        qvalue.update(sa, sample); // regular update

        sa = sa.get(); // clone
        // similarity update
        qvalue.updateSimilar(sa, sample);
    }

    /**
     * Gets the next move of the Qagent.
     *
     * @return the pair of new locations of the predators according to their next move.
     */
    @Override
    public Actions getNextMove() {
        this.lastAction = qLearning(currentState);

        return this.lastAction;
    }

    /**
     * Checks if we want to take risk and explore the world.
     *
     * @return true if we want to explore the world, false otherwise
     */
    public boolean explore() {
        Random rand = new Random();
        return rand.nextInt(100) <= 10;
    }

    /**
     *
     * Q-learning algorithm of the q-agent.
     *
     * @return next action.
     */
    private Actions qLearning(State state) {
        Actions action;

        // if on training mode and decided to explore
        if (mode == SessionProxy.MODE_TRAIN && explore()) {
            action = randomAction(candidateActions(state));
        } else {
            action = bestAction(state, candidateActions(state));
        }
        this.lastStateAction = new StateAction(state, action);
        return action;
    }

    /**
     *
     * @return next random action.
     */
    public Actions randomAction(List<Actions> actions) {
        Random rand = new Random();
        return actions.get(rand.nextInt(actions.size()));
    }

    /**
     * @param gameState
     * @return available pair-moves the predators can make
     */
    private List<Actions> candidateActions(State gameState) {
        ArrayList<Actions> actions = getAvailableMoves(gameState.getPredatorPosition(0), gameState.getPredatorPosition(1));
        return actions;
    }

    /**
     * @param location1
     * @param location2
     * @return true if the locations are legal and are empty
     */
    public boolean isEmpty(Point location1, Point location2) {
        return isInBounds(location1) && isInBounds(location2) &&
                !location1.equals(location2);
    }

    /**
     * @param location
     * @return true if the location is in the board bounds
     */
    public boolean isInBounds(Point location) {
        return location.getX() >= 0 && location.getX() <width && location.getY() >= 0 && location.getY() < height;
    }

    /**
     * @param predatorPosition1
     * @param predatorPosition2
     * @return all the actions the predators can make together
     */
    private ArrayList<Actions> getAvailableMoves(Point predatorPosition1, Point predatorPosition2) {
        ArrayList<Actions> moves = new ArrayList<>();

        List<Actions> candidateMoves = Action.actionsSet();

        for (Actions pair : candidateMoves) {
            if (isEmpty(predatorPosition1.translate(pair.getA1().getAction()), predatorPosition2) &&
                    isEmpty(predatorPosition1.translate(pair.getA1().getAction()), predatorPosition2.translate(pair.getA2().getAction()))) {
                moves.add(pair);
            }
        }
        return moves;
    }

    /**
     *
     * @return best next action according to Q-values.
     */
    public Actions bestAction(State gameState, List<Actions> actions) {
        List<Actions> best_actions = qvalue.getBestAction(gameState, actions);
        return randomAction(best_actions);
    }

    /**
     * Saves the estimated qvalues of the agents to a file.
     *
     */
    @Override
    public void keepKnowledge(String file) {
        try{

            FileOutputStream fout = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(this.qvalue.getValues());
            oos.close();
            System.out.println("Done");

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    /**
     * Loads the estimated values to the agents qvalue.
     *
     */
    @Override
    public void loadKnowledge(String file) {
        Map<StateAction, Double> values;
        Map<StateAction, Double> a = qvalue.getValues();
        try
        {
            FileInputStream fileIn = new FileInputStream(file);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            values = (Map<StateAction, Double>) in.readObject();
            in.close();
            fileIn.close();
        }catch(IOException i)
        {
            i.printStackTrace();
            return;
        }catch(ClassNotFoundException c)
        {
            System.out.println("Values not found");
            c.printStackTrace();
            return;
        }
        qvalue.loadValues(values);
    }

    @Override
    public void setMode(int mode) {
        this.mode = mode;
    }

    /**
     * @return the qvalue table
     */
    public Qvalue getQvalue() {
        return qvalue;
    }
}
