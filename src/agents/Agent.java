package agents;

import game.SessionProxy;

/**
 * Interface Agent - represents the basic requirements of any agent.
 */
public interface Agent {
    /**
     * Keeps the agent's current knowledge to a given file.
     *
     * @param file the file to keep the knowledge to
     */
    void keepKnowledge(String file);

    /**
     * Loads knowledge from a given file.
     *
     * @param file the file to load knowledge from
     */
    void loadKnowledge(String file);

    /**
     * Sets the agentMode of the agent.
     *
     * @param agentMode the new agentMode
     */
    void setMode(int agentMode);

    /**
     * Prepares the agent to play.
     */
    void prepare();
}
