package game;

import agents.Action;
import agents.Actions;
import agents.State;
import utils.Pair;

/**
 * Interface Game - represents a game functionality.
 */
public interface Game {

    /**
     * Prepares the game.
     */
    void prepare();

    /**
     * Does the next iteration of the game.
     */
    void doNextIter();

    /**
     * Returns the current state of the game.
     *
     * @param win indicates if there was a win
     */
    State getState(boolean win);

    /**
     * Applies the given move to the ith player.
     *
     * @param action the action to play
     */
    void applyMove(Actions action);

    /**
     * Draws the game.
     */
    void draw();

    /**
     * Checks if the game is over.
     *
     * @return true if the game is over, false otherwise
     */
    boolean isOver();
}
