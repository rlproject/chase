package game;

import agents.Action;
import agents.Actions;
import utils.Point;

import java.util.ArrayList;
import java.util.List;

/**
 * Class game.Environment - represents the world.
 */
public class Environment {
    private static final char EMPTY = ' ';  // rep an empty cell
    private char[][] env;                   // the environment

    /**
     * Constructs an environment of the given size.
     *
     * @param width the width
     * @param height the height
     */
    public Environment(int width, int height) {
        env = new char[height][width];
        for (int i = 0; i < env.length; i++) {
            for (int j = 0; j < env[i].length; j++) {
                env[i][j] = EMPTY;
            }
        }
    }

    /**
     * Checks if the given location in the environment is empty.
     *
     * @return true if the location is empty, false otherwise
     */
    public boolean isEmpty(Point location1, Point location2) {
        return isInBounds(location1) && isInBounds(location2) &&
                !(location1.equals(location2));
    }

    /**
     * Clears a given location.
     *
     * @param position the location to clear
     */
    public void clear(Point position) {
        env[position.getY()][position.getX()] = EMPTY;
    }

    /**
     * Sets a give location to a given char.
     *
     * @param location the location
     * @param ch the char
     */
    public void set(Point location, char ch) {
        env[location.getY()][location.getX()] = ch;
    }

    /**
     * Shows the current environment.
     */
    public void show() {
        for (int i = 0; i < 2 * getWidth(); i++)
            System.out.print("-");
        System.out.println();

        for (int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                System.out.print(env[i][j] + " ");
            }

            System.out.println();
        }

        for (int i = 0; i < 2 * getWidth(); i++)
            System.out.print("-");
        System.out.println();
    }

    /**
     * Checks if a give location is in the bounds of the environment.
     *
     * @param location the location to check
     * @return true if the location is in bounds, false otherwise
     */
    public boolean isInBounds(Point location) {
        return location.getX() >= 0 && location.getX() < getWidth() && location.getY() >= 0 && location.getY() < getHeight();
    }

    /**
     * Gets the width of the environment.
     *
     * @return the width of the environment.
     */
    public int getWidth() {
        return env[0].length;
    }

    /**
     * Gets the height of the environment.
     *
     * @return the height of the environment.
     */
    public int getHeight() {
        return env.length;
    }

    /**
     * Gets a list of the available moves.
     *
     * @return a list of the available and legal moves
     */
    public ArrayList<Actions> getAvailableMoves(Point position1, Point position2) {
        ArrayList<Actions> moves = new ArrayList<>();

        List<Actions> candidateMoves = Action.actionsSet();

        for (Actions pair : candidateMoves) {
            if (isEmpty(position1.translate(pair.getA1().getAction()), position2) &&
                    isEmpty(position1.translate(pair.getA1().getAction()), position2.translate(pair.getA2().getAction()))) {
                moves.add(pair);
            }
        }
        return moves;
    }

    /**
     * @param position - prey's current position
     * @return List of available moves
     */
    public ArrayList<Action> getPreyAvailableMoves(Point position) {
        ArrayList<Action> moves = new ArrayList<>();

        moves.add(Action.stay());

        if (isPreyEmpty(position.translate(Action.left().getAction()))) {
            moves.add(Action.left());
        }

        if (isPreyEmpty(position.translate(Action.right().getAction()))) {
            moves.add(Action.right());
        }

        if (isPreyEmpty(position.translate(Action.up().getAction()))) {
            moves.add(Action.up());
        }

        if (isPreyEmpty(position.translate(Action.down().getAction()))) {
            moves.add(Action.down());
        }

        return moves;
    }

    /**
     * @param location - prey's location
     * @return true if the given location is empty
     */
    public boolean isPreyEmpty(Point location) {
        return isInBounds(location) && env[location.getY()][location.getX()] == EMPTY;
    }

}
