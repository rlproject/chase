package game;

import agents.*;
import utils.Pair;

/**
 * Class Session - belongs to the operator. Temporally here.
 */
public class Session implements SessionProxy {
    private static final char PREY_SYMBOL = 'P';

    private Game game;
    private QAgent agents;
    private int movesCount;
    private Similarities sim;
    private boolean isDrawable;

    /**
     * Constructs the session.
     */
    public Session() {
        this.sim = null;
        this.isDrawable = false;
    }

    public void create(final int WIDTH, int HEIGHT) {
        if (this.sim == null) {
            this.sim = new Similarities(WIDTH, HEIGHT);
        }
        State.init(WIDTH, HEIGHT); // "global" width and height for states
        this.game = new ChaseGame(WIDTH, HEIGHT, PREY_SYMBOL);
        this.agents = new QAgent(WIDTH, HEIGHT, sim);
    }

    /**
     * Runs the game session.
     */
    public void run() {
        initialize();

        while (!game.isOver()) {

            // agent move
            // observe and let the other agent know about what you saw
            State gameState = game.getState(false);
            agents.observe(gameState);

            Actions action = agents.getNextMove();
            game.applyMove(action);

            // if won after first move
            if (game.isOver()) {
                agents.observe(game.getState(true));
                break;
            }

            // game move
            game.doNextIter();

            if (isDrawable) {
                game.draw();
            }

            // count move
            this.movesCount++;
        }
    }

    /**
     * Initializes the session.
     */
    private void initialize() {
        this.movesCount = 0;

        game.prepare();
        agents.prepare();
        agents.observe(game.getState(false));
    }

    /**
     * Gets the moves count.
     *
     * @return the moves count
     */
    public int getMovesCount() {
        return this.movesCount;
    }

    @Override
    public int getProgression() {
        return 0;
    }

    @Override
    public int getWinCount() {
        return 0;
    }

    @Override
    public int getKillsCount() {
        return 0;
    }

    @Override
    public void setMode(int agentMode) {
        agents.setMode(agentMode);
    }

    @Override
    public void setAgentType(int agentType) {
        if (agentType == TYPE_SIMILARITY) {
            this.sim.load("table.ser");
        } else {
            this.sim.unload();
        }
    }

    @Override
    public void setDrawable(boolean isDrawable) {
        this.isDrawable = isDrawable;
    }

    public QAgent getAgent() {
        return this.agents;
    }
}
