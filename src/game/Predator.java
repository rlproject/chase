package game;

import utils.Point;

/**
 * Class game.Predator - represents a single predator.
 */
public class Predator extends Player {
    public Predator(char symbol, Point position) {
        super(symbol, position);
    }
}
