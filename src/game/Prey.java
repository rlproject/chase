package game;

import utils.Point;

/**
 * Class game.Prey - represents the prey.
 */
public class Prey extends Player {
    public Prey(char symbol, Point position) {
        super(symbol, position);
    }
}
