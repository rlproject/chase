package game;

/**
 * Class IllegalMoveException - represents an illegal setPosition attempt.
 */
public class IllegalMoveException extends Exception {
}
