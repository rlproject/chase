package game;

import utils.Point;

/**
 * Class game.Player - represents a player that can setPosition in the game.
 */
public class Player {
    private char symbol;        // the symbol
    protected Point position;   // the position

    /**
     * Contractor.
     *
     * @param symbol the symbol
     * @param position the position
     */
    public Player(char symbol, Point position) {
        this.symbol = symbol;
        this.position = position;
    }

    /**
     * Gets the position.
     *
     * @return the position
     */
    public Point getPosition() {
        return position;
    }

    /**
     * Gets the symbol.
     *
     * @return the symbol
     */
    public char getSymbol() {
        return symbol;
    }

    /**
     * Sets the position of the player.
     *
     * @param position the new position
     */
    public void setPosition(Point position) {
        this.position = position;
    }
}
