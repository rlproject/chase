package game;

import agents.Action;
import agents.Actions;
import agents.Reward;
import agents.State;
import utils.Point;

import java.util.ArrayList;
import java.util.Random;

/**
 * Class ChaseGame - runs the game.
 */
public class ChaseGame implements Game {
    public enum Turn {PREDATOR, PREY}
    private static final int NUM_OF_PREDATORS = 2;

    private Environment environment;    // the environment
    private Prey prey;                  // the prey
    private Predator[] predators;       // the predators
    private Turn turn;                  // current turn
    private boolean isOver;             // is game over

    /**
     * Constructs the game.
     */
    public ChaseGame(int width, int height, char preySym) {
        environment = new Environment(width, height);
        Random rand = new Random();

        prey = new Prey(preySym, new Point(rand.nextInt(environment.getWidth() - 1), rand.nextInt(environment.getWidth() - 1)));
        predators = new Predator[NUM_OF_PREDATORS];
        turn = Turn.PREDATOR;
        isOver = false;
        initialize();
    }

    /**
     * Initializes the game.
     */
    private void initialize() {
        addPlayer(prey);
        ArrayList<Point> predatorsPos = new ArrayList<>();
        predatorsPos.add(new Point(0, environment.getHeight() - 2));
        predatorsPos.add(new Point(1, environment.getHeight() - 1));

        for (int i = 0; i < predators.length; i++) {
            predators[i] = new Predator(("" + i).charAt(0), predatorsPos.get(i));
            addPlayer(predators[i]);
        }
    }

    /**
     * Restarts the game.
     */
    public void prepare() {
        environment = new Environment(environment.getWidth(), environment.getHeight());
        prey = new Prey(prey.getSymbol(), new Point(environment.getWidth() - 1, 0));
        predators = new Predator[NUM_OF_PREDATORS];
        turn = Turn.PREDATOR;
        isOver = false;
        initialize();
    }

    @Override
    public void doNextIter() {
        movePrey();
        nextTurn();
    }

    /**
     * Checks if the game is over.
     *
     * @return true if the game is over, false otherwise
     */
    public boolean isOver() {
        return isOver;
    }

    /**
     * Draws the game.
     */
    public void draw() {
        environment.show();
    }

    /**
     * Moves the predators.
     *
     * @param action the agent's move
     * @throws IllegalMoveException if one of the moves is illegal
     */
    private void movePredators(Actions action) throws IllegalMoveException {
        if (predators[0].getPosition().translate(action.getA1().getAction()).equals(prey.getPosition()) || predators[1].getPosition().translate(action.getA2().getAction()).equals(prey.getPosition())) {
            isOver = true;
            return;
        }
        movePlayerPredator(predators, action);
    }

    /**
     * Gets the best action for the prey.
     *
     * @return the best action of the prey
     */
    private Action getPreysBest() {
        ArrayList<Action> moves = environment.getPreyAvailableMoves(prey.getPosition());
        double max = -1;
        Action maxA = Action.stay();

        for (Action a : moves) {
            double distance = Reward.proximity(this.predators[0].getPosition(), this.prey.getPosition());
            distance += Reward.proximity(this.predators[1].getPosition(), this.prey.getPosition());

            if (distance > max) {
                max = distance;
                maxA = a;
            }

        }

        return maxA;
    }

    /**
     * Moves the prey randomly.
     */
    private void movePrey() {
        ArrayList<Action> moves = environment.getPreyAvailableMoves(prey.getPosition());
        Random rand = new Random();

        try {
            if(rand.nextInt(100) <= 20) { // 20% - randomly
                movePlayerPrey(prey, moves.get(rand.nextInt(moves.size())));
            } else { // 80% - evade
                movePlayerPrey(prey, getPreysBest());
            }
        } catch (IllegalMoveException e) {
            System.out.println("Prey could not move (impossible exception).");
        }
    }

    /**
     * Gets the game state.
     *
     * @param win the win state
     * @return the game state
     */
    public State getState(boolean win) {
        Point[] predatorPositions = new Point[NUM_OF_PREDATORS];
        for (int i = 0; i < NUM_OF_PREDATORS; ++i) {
            predatorPositions[i] = predators[i].getPosition().get();
        }
        return new State(prey.getPosition().get(), predatorPositions, win);
    }

    public void applyMove(Actions action) {
        try {
            movePredators(action);
            nextTurn();
        } catch (IllegalMoveException e) {
            System.out.println("Illegal move.");
            System.exit(0);
        }
    }

    /**
     * Adds a player to the game.
     *
     * @param player the player to add
     */
    public void addPlayer(Player player) {
        environment.set(player.getPosition(), player.getSymbol());
    }

    /**
     * Moves the a player.
     *
     * @param player the player to setPosition
     * @param action the direction to setPosition to
     * @throws IllegalMoveException if the setPosition is not possible
     */
    public void movePlayerPrey(Player player, Action action) throws IllegalMoveException {
        ArrayList<Action> moves = environment.getPreyAvailableMoves(player.position);
        Point newPosition = player.getPosition().translate(action.getAction());

        if (!moves.contains(action)) {
            throw new IllegalMoveException();
        }

        environment.clear(player.getPosition());
        environment.set(newPosition, player.getSymbol());
        player.setPosition(newPosition);
    }

    /**
     * Moves the a player.
     *
     * @param player the player to setPosition
     * @param action the direction to setPosition to
     * @throws IllegalMoveException if the setPosition is not possible
     */
    public void movePlayerPredator(Player[] player,  Actions action) throws IllegalMoveException {
        ArrayList<Actions> moves = environment.getAvailableMoves(player[0].position, player[1].position);
        Point newPosition1 = player[0].getPosition().translate(action.getA1().getAction());
        Point newPosition2 = player[1].getPosition().translate(action.getA2().getAction());

        if (!moves.contains(action)) {
            throw new IllegalMoveException();
        }

        environment.clear(player[0].getPosition());
        environment.clear(player[1].getPosition());
        environment.set(newPosition1, player[0].getSymbol());
        environment.set(newPosition2, player[1].getSymbol());
        player[0].setPosition(newPosition1);
        player[1].setPosition(newPosition2);
    }

    /**
     * Checks the current turn.
     *
     * @return true if it is the prey's turn, false otherwise
     */
    public boolean isPreyTurn() {
        return this.turn == Turn.PREY;
    }

    /**
     * Next turn.
     * Note: this is not how it should be done, but I'll leave that for now as it is.
     */
    private void nextTurn() {
        if (turn == Turn.PREDATOR) {
            turn = Turn.PREY;
        } else {
            turn = Turn.PREDATOR;
        }
    }
}
